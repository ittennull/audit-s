# Audit-s
Audit-s is an audit log collector with a simple UI. It is designed to be simple and small and fulfill basic requirements: collect read-only audit logs, save them and provide an API to query it.

It consists of two parts: _importer_ service (aka collector) and a _web-server_. The importer exposes a grpc endpoint that allows applications to push audit logs to it. The logs are saved in [MongoDB](https://www.mongodb.com) database. The web-server provides a HTTP API to query logs as well as a simple UI that utilizes the API to display the logs.

![diagram](docs/audit-s.svg)

The separation for two services _importer_ and _web-server_ is done to be able to scale them differently, the web-server probably won't have much load whereas importer can get busy.
