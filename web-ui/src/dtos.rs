use chrono::{DateTime, Utc};
use serde::Deserialize;
use std::collections::HashMap;

#[derive(Deserialize)]
pub struct AuditLogItem {
    pub id: String,
    pub ids: Vec<String>,
    pub source: String,
    pub message: String,
    pub operation_type: String,
    pub data: HashMap<String, String>,
    pub timestamp: DateTime<Utc>,
}
