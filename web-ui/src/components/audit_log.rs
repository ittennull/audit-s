use crate::components::FilterParameters;
use crate::dtos::AuditLogItem;
use crate::event_bus::EventBus;
use chrono::{DateTime, SecondsFormat, Utc};
use gloo_net::http::Request;
use yew::{classes, html, Component, Context, Html};
use yew_agent::{Bridge, Bridged};

pub struct AuditLog {
    items: Vec<AuditLogItem>,
    data_loaded: bool,
    loading_error: String,
    _producer: Box<dyn Bridge<EventBus>>,
}

pub enum Msg {
    Load(FilterParameters),
    AuditLoaded(Vec<AuditLogItem>),
    LoadingFailed(String),
    HideLoadingFailedError,
}

impl Component for AuditLog {
    type Message = Msg;
    type Properties = ();

    fn create(ctx: &Context<Self>) -> Self {
        Self {
            items: vec![],
            data_loaded: false,
            loading_error: String::default(),
            _producer: EventBus::bridge(ctx.link().callback(Msg::Load)),
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::Load(parameters) => {
                ctx.link().send_future(request_data(parameters));
            }
            Msg::AuditLoaded(items) => {
                self.items = items;
                self.data_loaded = true;
                self.loading_error.clear();
            }
            Msg::LoadingFailed(err) => {
                self.loading_error = err;
            }
            Msg::HideLoadingFailedError => {
                self.loading_error.clear();
            }
        }

        true
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let rows = self
            .items
            .iter()
            .map(|x| {
                let mut data = x.data.iter().collect::<Vec<_>>();
                data.sort_by_key(|(key, _)| *key);

                html! {
                    <tr key={x.id.as_str()}>
                        <td>
                            {x.ids.iter().map(|id| html! {<div>{id}</div>}).collect::<Vec<_>>()}
                        </td>
                        <td> {&x.operation_type} </td>
                        <td> {&x.source} </td>
                        <td> {&x.message} </td>
                        <td> {data.iter().map(|(k,v)| html! {
                            <div>
                                <span class="has-text-grey">{k}</span> {": "} {v}
                            </div>
                            }).collect::<Vec<_>>()}
                        </td>
                        <td> {x.timestamp.format("%Y-%m-%d %H:%M:%S")} </td>
                    </tr>
                }
            })
            .collect::<Vec<_>>();

        let hide_error = ctx.link().callback(|_| Msg::HideLoadingFailedError);

        html! {
            <>
            <div class="notification is-danger" hidden={self.loading_error.is_empty()}>
                <button class="delete" onclick={hide_error}></button>
                {&self.loading_error}
            </div>
            <table class={classes!("table", "is-striped", "is-hoverable", "is-fullwidth")}>
                <thead>
                    <th> {"IDs"} </th>
                    <th> {"Type"} </th>
                    <th> {"Source"} </th>
                    <th> {"Message"} </th>
                    <th> {"Data"} </th>
                    <th> {"Timestamp(UTC)"} </th>
                </thead>
                <tbody>
                    {rows}
                </tbody>
            </table>
            <div class="has-text-centered" hidden={!(self.data_loaded && self.items.is_empty())}>
                {"No matches"}
            </div>
            </>
        }
    }
}

async fn request_data(parameters: FilterParameters) -> Msg {
    let mut url = format!("/api/audit?ids={}", parameters.ids);
    let mut add_param = |name: &str, date: Option<DateTime<Utc>>| {
        if let Some(date) = date {
            url.push_str(&format!(
                "&{}={}",
                name,
                date.to_rfc3339_opts(SecondsFormat::Secs, true)
            ));
        }
    };

    add_param("from", parameters.from);
    add_param("to", parameters.to);

    match Request::get(&url).send().await {
        Ok(response) if response.ok() => match response.json::<Vec<AuditLogItem>>().await {
            Ok(items) => Msg::AuditLoaded(items),
            Err(e) => Msg::LoadingFailed(format!("{:?}", e)),
        },
        Ok(response) => Msg::LoadingFailed(format!("{:?}", response)),
        Err(e) => Msg::LoadingFailed(format!("{:?}", e)),
    }
}
