use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

pub mod audit_log;
pub mod filters;

#[derive(Clone, Deserialize, Serialize, Default)]
pub struct FilterParameters {
    pub ids: String,
    pub from: Option<DateTime<Utc>>,
    pub to: Option<DateTime<Utc>>,
}

impl FilterParameters {
    fn new(ids: String, from: Option<DateTime<Utc>>, to: Option<DateTime<Utc>>) -> Self {
        Self { ids, from, to }
    }
}
