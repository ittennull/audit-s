use crate::components::FilterParameters;
use crate::event_bus::{EventBus, Request};
use crate::routes::Route;
use chrono::{DateTime, NaiveDateTime, Utc};
use web_sys::{HtmlInputElement, KeyboardEvent};
use yew::prelude::*;
use yew::{html, Component, Context, FocusEvent, Html};
use yew_agent::{Dispatched, Dispatcher};
use yew_router::prelude::*;

pub struct Filters {
    event_bus: Dispatcher<EventBus>,
    ids: String,
    from: Option<DateTime<Utc>>,
    to: Option<DateTime<Utc>>,
    show_error_from_less_than_to: bool,
}

pub enum Msg {
    Load,
    SetIds(String),
    SetFrom(Option<DateTime<Utc>>),
    SetTo(Option<DateTime<Utc>>),
}

impl Component for Filters {
    type Message = Msg;
    type Properties = ();

    fn create(ctx: &Context<Self>) -> Self {
        let parameters = ctx.link().location().map(|x| x.query::<FilterParameters>());
        let parameters = parameters.map_or(FilterParameters::default(), |x| x.unwrap_or_default());
        let show_error_from_less_than_to = !validate_from_and_to(&parameters.from, &parameters.to);
        let mut event_bus = EventBus::dispatcher();

        if !parameters.ids.is_empty() && !show_error_from_less_than_to {
            event_bus.send(Request::Load(parameters.clone()));
        }

        Self {
            event_bus,
            ids: parameters.ids,
            from: parameters.from,
            to: parameters.to,
            show_error_from_less_than_to,
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        use Msg::*;

        match msg {
            Load => {
                let ids = self
                    .ids
                    .split(",")
                    .map(|x| x.trim())
                    .collect::<Vec<_>>()
                    .join(",");
                if !ids.is_empty() {
                    let parameters = FilterParameters::new(ids, self.from, self.to);
                    ctx.link()
                        .history()
                        .map(|x| x.push_with_query(Route::Home, &parameters));
                    self.event_bus.send(Request::Load(parameters));
                }
                false
            }
            SetIds(ids) => {
                self.ids = ids;
                true
            }
            SetFrom(from) => {
                self.from = from;
                self.show_error_from_less_than_to = !validate_from_and_to(&self.from, &self.to);
                true
            }
            SetTo(to) => {
                self.to = to;
                self.show_error_from_less_than_to = !validate_from_and_to(&self.from, &self.to);
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let load_click = ctx.link().callback(|_| Msg::Load);
        let ids_onkeyup = ctx.link().callback(move |e: KeyboardEvent| {
            if e.code() == "Enter" {
                Msg::Load
            } else {
                let input_element: HtmlInputElement = e.target_unchecked_into();
                Msg::SetIds(input_element.value())
            }
        });

        let set_datetime = |msg: fn(Option<DateTime<Utc>>) -> Msg| {
            ctx.link().callback(move |e: FocusEvent| {
                let input_element: HtmlInputElement = e.target_unchecked_into();
                let value = input_element.value();
                if value.is_empty() {
                    msg(None)
                } else {
                    let datetime = NaiveDateTime::parse_from_str(value.as_str(), "%Y-%m-%dT%H:%M")
                        .map(|datetime| Some(DateTime::from_utc(datetime, Utc)))
                        .expect("Incorrect datetime format");
                    msg(datetime)
                }
            })
        };

        let clear_datetime =
            |msg: fn(Option<DateTime<Utc>>) -> Msg| ctx.link().callback(move |_| msg(None));

        let datetime_value = |x: &Option<DateTime<Utc>>| {
            x.map(|x| x.format("%Y-%m-%dT%H:%M").to_string())
                .unwrap_or_default()
        };

        let hide_error_class = match self.show_error_from_less_than_to {
            true => None,
            false => Some("is-hidden"),
        };

        html! {
            <div class="field is-horizontal">
                <div class="field-body">
                    <div class="field">
                        <p class="control is-expanded">
                            <input class="input" type="text" value={self.ids.clone()} placeholder="id1, id2, id3" onkeyup={ids_onkeyup} />
                        </p>
                    </div>

                    <div class="field-label is-normal">
                        <label class="label">{"From"}</label>
                    </div>
                    <div class="field">
                        <p class="is-flex">
                            <p class="control ">
                                <input class="input" type="datetime-local" value={datetime_value(&self.from)} onblur={set_datetime(Msg::SetFrom)} />
                            </p>
                            <button class="button is-outlined" onclick={clear_datetime(Msg::SetFrom)} title="clear">{"✕"}</button>
                        </p>
                        <p class={classes!("help", "is-danger", hide_error_class)}>
                            <b>{"From"}</b>{" can't be greater than "}<b>{"To"}</b>
                        </p>
                    </div>

                    <div class="field-label is-normal">
                        <label class="label">{"To"}</label>
                    </div>
                    <div class="field">
                        <p class="is-flex">
                            <p class="control ">
                                <input class="input" type="datetime-local" value={datetime_value(&self.to)} onblur={set_datetime(Msg::SetTo)} />
                            </p>
                            <button class="button is-outlined" onclick={clear_datetime(Msg::SetTo)} title="clear">{"✕"}</button>
                        </p>
                    </div>

                    <div class="field">
                        <div class="control">
                          <button class="button is-primary" onclick={load_click} disabled={self.ids.is_empty()}>{"Load"}</button>
                        </div>
                    </div>
                </div>
            </div>
        }
    }
}

fn validate_from_and_to(from: &Option<DateTime<Utc>>, to: &Option<DateTime<Utc>>) -> bool {
    match (from, to) {
        (Some(from), Some(to)) => from <= to,
        _ => true,
    }
}
