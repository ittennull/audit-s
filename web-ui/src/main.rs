mod components;
mod dtos;
mod event_bus;
mod routes;

use components::{audit_log::AuditLog, filters::Filters};
use std::panic;
use yew::prelude::*;
use yew_router::router::*;

struct Model {}

impl Component for Model {
    type Message = ();
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self {}
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        html! {
            <BrowserRouter>
                <div class="m-1">
                    <Filters/>
                    <AuditLog/>
                </div>
            </BrowserRouter>
        }
    }
}

fn main() {
    panic::set_hook(Box::new(console_error_panic_hook::hook));
    wasm_logger::init(wasm_logger::Config::default());

    yew::start_app::<Model>();
}
