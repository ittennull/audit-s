use anyhow::Result;
use bson::oid::ObjectId;
use chrono::Utc;
use std::sync::Arc;
use thingbuf::mpsc;
use thingbuf::mpsc::{Receiver, Sender};
use tonic::transport::Server;
use tonic::{Request, Response, Status};

mod pb {
    tonic::include_proto!("audit_log");
}

use crate::store::AuditLogItem;
use crate::Store;
use pb::audit_log_server::{AuditLog, AuditLogServer};

struct AuditLogService {
    tx: Sender<AuditLogItem>,
}

#[tonic::async_trait]
impl AuditLog for AuditLogService {
    async fn save(&self, request: Request<pb::AuditLogItem>) -> Result<Response<()>, Status> {
        let item = make_audit_log_item(request.into_inner());
        self.tx
            .send(item)
            .await
            .map_err(|err| Status::internal(format!("Can't queue an item: {}", err)))?;

        Ok(Response::new(()))
    }
}

pub async fn start(store: Arc<Store>) -> Result<()> {
    let addr = "[::1]:5005".parse().unwrap();
    let (tx, rx) = mpsc::channel::<AuditLogItem>(1000);
    let audit_log_service = AuditLogService { tx };

    tokio::spawn(async {
        save_audit_thread(rx, store).await;
    });

    println!("AuditLogServer listening on {}", addr);

    Server::builder()
        .add_service(AuditLogServer::new(audit_log_service))
        .serve(addr)
        .await?;

    Ok(())
}

async fn save_audit_thread(rx: Receiver<AuditLogItem>, store: Arc<Store>) {
    while let Some(item) = rx.recv().await {
        if let Err(err) = store.save(&item).await {
            println!("Save error: {}", err);
        }
    }
}

fn make_audit_log_item(request: pb::AuditLogItem) -> AuditLogItem {
    AuditLogItem {
        id: ObjectId::new(),
        ids: request.ids,
        source: request.source,
        message: request.message,
        operation_type: request.operation_type,
        data: request.data,
        timestamp: Utc::now(),
    }
}
