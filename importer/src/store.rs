use anyhow::Result;
use chrono::prelude::*;
use mongodb::bson::oid::ObjectId;
use mongodb::options::ClientOptions;
use mongodb::{Client, Collection};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AuditLogItem {
    #[serde(rename = "_id")]
    pub id: ObjectId,

    pub ids: Vec<String>,
    pub source: String,
    pub message: String,
    pub operation_type: String,
    pub data: HashMap<String, String>,

    #[serde(with = "bson::serde_helpers::chrono_datetime_as_bson_datetime")]
    pub timestamp: DateTime<Utc>,
}

pub struct Store {
    collection: Collection<AuditLogItem>,
}

impl Store {
    pub async fn new(connection_string: &str) -> Result<Self> {
        let mut client_options = ClientOptions::parse(connection_string).await?;
        client_options.app_name = Some("audit-s".to_string());
        let client = Client::with_options(client_options)?;
        let db = client.database("audit");
        let collection = db.collection::<AuditLogItem>("audit_log");

        Ok(Self { collection })
    }

    pub async fn save(&self, item: &AuditLogItem) -> Result<()> {
        self.collection.insert_one(item, None).await?;
        Ok(())
    }
}

impl Default for AuditLogItem {
    fn default() -> Self {
        Self {
            id: ObjectId::new(),
            ids: vec![],
            source: "".to_string(),
            message: "".to_string(),
            operation_type: "".to_string(),
            data: Default::default(),
            timestamp: Utc::now(),
        }
    }
}
