extern crate core;

use crate::store::Store;
use anyhow::anyhow;
use std::sync::Arc;

mod grpc_server;
mod store;

const MONGODB_CONNECTION_STRING_NAME: &'static str = "MONGODB_CONNECTION_STRING";

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let connection_string = std::env::vars()
        .find(|(name, _)| name == MONGODB_CONNECTION_STRING_NAME)
        .map(|(_, value)| value)
        .ok_or(anyhow!(
            "Missing {} environment variable",
            MONGODB_CONNECTION_STRING_NAME
        ))?;

    let store = Store::new(&connection_string).await?;
    grpc_server::start(Arc::new(store)).await?;
    Ok(())
}
