FROM rust AS build
RUN apt-get update && apt-get install -y cmake
RUN cargo install --locked trunk && \
    cargo install wasm-bindgen-cli && \
    rustup target add wasm32-unknown-unknown
WORKDIR /src
COPY . .
RUN cargo build --release
WORKDIR /src/web-ui
RUN trunk build --release

FROM debian:buster-slim AS importer
WORKDIR /app
COPY --from=build /src/target/release/audit-s-importer ./
ENTRYPOINT ./audit-s-importer

FROM debian:buster-slim AS web-server
WORKDIR /app
COPY --from=build /src/target/release/audit-s-web-server ./
COPY --from=build /src/web-ui/dist ./web-ui/dist/
ENTRYPOINT ./audit-s-web-server