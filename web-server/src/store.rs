use anyhow::Result;
use bson::doc;
use chrono::prelude::*;
use futures::stream::TryStreamExt;
use mongodb::bson::oid::ObjectId;
use mongodb::options::{ClientOptions, FindOptions};
use mongodb::{Client, Collection};
use nameof::name_of;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AuditLogItem {
    #[serde(rename = "_id")]
    pub id: ObjectId,

    pub ids: Vec<String>,
    pub source: String,
    pub message: String,
    pub operation_type: String,
    pub data: HashMap<String, String>,

    #[serde(with = "bson::serde_helpers::chrono_datetime_as_bson_datetime")]
    pub timestamp: DateTime<Utc>,
}

#[derive(Clone)]
pub struct Store {
    collection: Collection<AuditLogItem>,
}

impl Store {
    pub async fn new(connection_string: &str) -> Result<Self> {
        let mut client_options = ClientOptions::parse(connection_string).await?;
        client_options.app_name = Some("audit-s".to_string());
        let client = Client::with_options(client_options)?;
        let db = client.database("audit");
        let collection = db.collection::<AuditLogItem>("audit_log");

        Ok(Self { collection })
    }

    pub async fn find(
        &self,
        ids: &Vec<String>,
        from: Option<DateTime<Utc>>,
        to: Option<DateTime<Utc>>,
    ) -> Result<Vec<AuditLogItem>> {
        let mut filter = doc! { name_of!(ids in AuditLogItem): { "$in": ids } };

        let mut time_filter = doc! {};
        if let Some(to) = to {
            time_filter.insert("$lt", to);
        }
        if let Some(from) = from {
            time_filter.insert("$gt", from);
        }

        if !time_filter.is_empty() {
            filter.insert(name_of!(timestamp in AuditLogItem), time_filter);
        }

        let find_options = FindOptions::builder()
            .sort(doc! { name_of!(timestamp in AuditLogItem): -1 })
            .build();
        let result = self.collection.find(filter, find_options).await?;
        let docs: Vec<_> = result.try_collect().await?;
        Ok(docs)
    }
}
