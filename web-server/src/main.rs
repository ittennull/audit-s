mod api;
mod store;

use crate::api::find_audit_log_items;
use crate::store::Store;
use actix_files::Files;
use actix_web::{web, App, HttpServer};
use anyhow::{anyhow, Result};

const MONGODB_CONNECTION_STRING_NAME: &'static str = "MONGODB_CONNECTION_STRING";

#[actix_web::main]
async fn main() -> Result<()> {
    let connection_string = std::env::vars()
        .find(|(name, _)| name == MONGODB_CONNECTION_STRING_NAME)
        .map(|(_, value)| value)
        .ok_or(anyhow!(
            "Missing {} environment variable",
            MONGODB_CONNECTION_STRING_NAME
        ))?;
    let store = Store::new(&connection_string).await?;

    HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(store.clone()))
            .service(web::scope("/api").service(find_audit_log_items))
            .service(Files::new("/", "./web-ui/dist/").index_file("index.html"))
    })
    .bind(("127.0.0.1", 8000))?
    .run()
    .await
    .map_err(|e| anyhow!("http server error: {:?}", e))
}
