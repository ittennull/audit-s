use crate::Store;
use actix_web::error::ErrorBadRequest;
use actix_web::web::{Json, Query};
use actix_web::{get, web};
use anyhow::Result;
use chrono::{DateTime, NaiveDate, NaiveDateTime, NaiveTime, Utc};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[get("/audit")]
pub async fn find_audit_log_items(
    query: Query<FindAuditLogItemsQueryParameters>,
    store: web::Data<Store>,
) -> Result<Json<Vec<AuditLogItemDto>>, actix_web::Error> {
    let ids = read_ids(&query.ids)?;
    let from = read_datetime(query.from.as_ref())?;
    let to = read_datetime(query.to.as_ref())?;
    if let (Some(from), Some(to)) = (from, to) {
        if from > to {
            return Err(ErrorBadRequest("Value 'from' must be less than value 'to'"));
        }
    }

    let items = store.find(&ids, from, to).await.map_err(ErrorBadRequest)?;
    let dtos = items
        .into_iter()
        .map(|x| AuditLogItemDto {
            id: x.id.to_string(),
            ids: x.ids,
            source: x.source,
            message: x.message,
            operation_type: x.operation_type,
            data: x.data,
            timestamp: x.timestamp,
        })
        .collect::<Vec<_>>();
    Ok(Json(dtos))
}

#[derive(Serialize)]
pub struct AuditLogItemDto {
    pub id: String,
    pub ids: Vec<String>,
    pub source: String,
    pub message: String,
    pub operation_type: String,
    pub data: HashMap<String, String>,
    pub timestamp: DateTime<Utc>,
}

#[derive(Deserialize)]
pub struct FindAuditLogItemsQueryParameters {
    ids: String,
    from: Option<String>,
    to: Option<String>,
}

pub fn read_ids(input: &str) -> Result<Vec<String>, actix_web::error::Error> {
    if input.is_empty() {
        return Err(ErrorBadRequest("ids parameter can't be empty"));
    }

    let ids = input.split(',').map(|x| x.to_string()).collect::<Vec<_>>();
    Ok(ids)
}

fn read_datetime(input: Option<&String>) -> Result<Option<DateTime<Utc>>, actix_web::error::Error> {
    match input {
        Some(input) => NaiveDateTime::parse_from_str(input, "%Y-%m-%dT%H:%M:%SZ")
            .or_else(|_| NaiveDateTime::parse_from_str(input, "%Y-%m-%dT%H:%M:%S"))
            .or_else(|_| NaiveDateTime::parse_from_str(input, "%Y-%m-%dT%H:%M"))
            .or_else(|_| {
                NaiveDate::parse_from_str(input, "%Y-%m-%d")
                    .map(|date| NaiveDateTime::new(date, NaiveTime::from_hms(0, 0, 0)))
            })
            .map(|datetime| Some(DateTime::from_utc(datetime, Utc)))
            .map_err(|_| {
                ErrorBadRequest(format!(
                    "Failed to parse value: '{}'
The value is invalid or not one of the supported formats:
yyyy-MM-ddThh:mm:ss
yyyy-MM-ddThh:mm
yyyy-MM-dd",
                    input
                ))
            }),
        None => Ok(None),
    }
}
